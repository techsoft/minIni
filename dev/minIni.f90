!

module minIniLib
  use, intrinsic :: iso_c_binding
  implicit none

  interface
  !
  !>  Gets Name of Section with index idx
  !!
    function ini_getsection (idx, Buffer, BufferSize, Filename) result(numchar) bind(C)
    !C-arguments: int idx, TCHAR *Buffer, int BufferSize, const TCHAR *Filename)
      import :: c_int, c_char
      integer(kind=c_int), intent(in), value :: idx, BufferSize
      character(kind=c_char, len=1), intent(in out) :: Buffer(*)
      character(kind=c_char, len=1), intent(in) :: Filename(*)
      integer(kind=c_int) :: numchar
    end function ini_getsection
    !
    !>  Gets Key name in Section
    !!
    function ini_getkey(Section, idx, Buffer, BufferSize, Filename) result(numchar) bind(C)
    !C-arguments: const TCHAR *Section, int idx, TCHAR *Buffer, int BufferSize, const TCHAR *Filename
      import :: c_char, c_int
      character(kind=c_char, len=1), intent(in) :: Section(*), Filename(*)
      character(kind=c_char, len=1), intent(in out) :: Buffer(*)
      integer(kind=c_int), intent(in), value :: idx, BufferSize
      integer(kind=c_int) :: numchar
    end function ini_getkey
    !
    !!  ini_gets() Gets String value of Key in Section
    !!
    !! \param Section     the name of the section to search for
    !! \param Key         the name of the entry to find the value of
    !! \param DefValue    default string in the event of a failed read
    !! \param Buffer      a pointer to the buffer to copy into
    !! \param BufferSize  the maximum number of characters to copy
    !! \param Filename    the name and full path of the .ini file to read from
    !!
    !! \return            the number of characters copied into the supplied buffer
    function ini_gets(Section, Key, DefValue, Buffer, BufferSize, Filename) result(numchar) bind(C)
    !C-interface:
    !
    !int ini_gets(const TCHAR *Section, const TCHAR *Key, const TCHAR *DefValue,
    !         TCHAR *Buffer, int BufferSize, const TCHAR *Filename)
    !
      import :: c_char, c_int
      character(kind=c_char, len=1), intent(in) :: Section(*), Key(*), DefValue(*), Filename(*)
      character(kind=c_char, len=1), intent(in out) :: Buffer(*)
      integer(kind=c_int), intent(in), value :: BufferSize
      integer(kind=c_int) :: numchar
    end function ini_gets
    !
    !> ini_getl()  Gets long integer value of Key in Section
    !!
    !! \param Section     the name of the section to search for
    !! \param Key         the name of the entry to find the value of
    !! \param DefValue    the default value in the event of a failed read
    !! \param Filename    the name of the .ini file to read from
    !!
    !! \return            the value located at Key
    function ini_getl(Section, Key, DefValue, Filename) result(KeyValue) bind(C)
    !C-interface:
    !
    !long ini_getl(const TCHAR *Section, const TCHAR *Key, long DefValue, const TCHAR *Filename)
    !
      import :: c_char, c_long
      character(kind=c_char, len=1), intent(in) :: Section(*), Key(*), Filename(*)
      integer(kind=c_long), intent(in), value :: DefValue
      integer(kind=c_long) :: KeyValue
    end function ini_getl
    !
    !> ini_getbool()
    !! \brief
    !! \param Section     the name of the section to search for
    !! \param Key         the name of the entry to find the value of
    !! \param DefValue    default value in the event of a failed read; it should
    !!                    zero (0) or one (1).
    !! \param Buffer      a pointer to the buffer to copy into
    !! \param BufferSize  the maximum number of characters to copy
    !! \param Filename    the name and full path of the .ini file to read from
    !!
    !! \details A true boolean is found if one of the following is matched:
    !! - A string starting with 'y' or 'Y'
    !! - A string starting with 't' or 'T'
    !! - A string starting with '1'
    !!
    !! A false boolean is found if one of the following is matched:
    !! - A string starting with 'n' or 'N'
    !! - A string starting with 'f' or 'F'
    !! - A string starting with '0'
    !!
    !! \return            the true/false flag as interpreted at Key
    function ini_getbool(Section, Key, DefValue, Filename) result(KeyValue) bind(C)
    !C-interface:
    !
    !int ini_getbool(const TCHAR *Section, const TCHAR *Key, int DefValue, const TCHAR *Filename)
    !
      import :: c_char, c_int, c_bool
      character(kind=c_char, len=1), intent(in) :: Section(*), Key(*), Filename(*)
      integer(kind=c_int), intent(in), value :: DefValue
      logical(kind=c_bool) :: KeyValue
    end function ini_getbool
#if !defined(INI_READONLY)
    !> ini_puts()
    !! \param Section     the name of the section to write the string in
    !! \param Key         the name of the entry to write, or NULL to erase all keys in the section
    !! \param Value       a pointer to the buffer the string, or NULL to erase the key
    !! \param Filename    the name and full path of the .ini file to write to
    !!
    !! \return            1 if successful, otherwise 0
    !!
    function ini_puts( Section, Key, Valu, Filename) result(success) bind(C)
    !C-interface
    !
    !int   ini_puts(const mTCHAR *Section, const mTCHAR *Key, const mTCHAR *Value, const mTCHAR *Filename);
    !
      import :: c_char, c_int
      character(kind=c_char, len=1), intent(in) :: Section(*), Key(*), Valu(*), Filename(*)
      integer(c_int) :: success !1 if successful, otherwise 0
    end function
    !> ini_putl()
    !!  \param Section     the name of the section to write the value in
    !!  \param Key         the name of the entry to write
    !!  \param Value       the value to write
    !!  \param Filename    the name and full path of the .ini file to write to
    !!
    !!  \return            1 if successful, otherwise 0
    !!
    function ini_putl (Section, Key, Valu, Filename) result(success) bind(C)
    !C-interface
    !
    !int ini_putl(const TCHAR *Section, const TCHAR *Key, long Value, const TCHAR *Filename)
      import :: c_char, c_long, c_int
      character(kind=c_char, len=1), intent(in) :: Section(*), Key(*), Filename(*)
      integer(kind=c_long) :: Valu
      integer(c_int) :: success !1 if successful, otherwise 0
    end function
    !
    !> ini_putd()
    !!
    function ini_putd(Section, Key, dValue, Filename) bind(C)
    !C-interface:
    !int ini_putf(const TCHAR *Section, const TCHAR *Key, INI_REAL64 Value, const TCHAR *Filename)
      import :: c_char, c_double
      character(kind=c_char, len=1), intent(in) :: Section(*), Key(*), Filename(*)
      real(kind=c_double), intent(in), value :: dValue
    end function ini_putd
    !
    !> ini_putf()
    !!
    function ini_putf(Section, Key, fValue, Filename) bind(C)
    !C-interface:
    !int ini_putf(const TCHAR *Section, const TCHAR *Key, INI_REAL Value, const TCHAR *Filename)
      import :: c_char, c_float
      character(kind=c_char, len=1), intent(in) :: Section(*), Key(*), Filename(*)
      real(kind=c_float), intent(in), value :: fValue
    end function ini_putf
#endif
  end interface

  interface
    !
    ! INI_REAL is defined in minIniGlue.h at compilation of minIni C-library (default to float)
    !
    !> ini_getf()
    !! \param Section     the name of the section to search for
    !! \param Key         the name of the entry to find the value of
    !! \param DefValue    the default value in the event of a failed read
    !! \param Filename    the name of the .ini file to read from
    !!
    !! \return            the value located at Key
    function ini_getf(Section, Key, DefValue, Filename) result(KeyValue) bind(C)
    !C-interface:
    !
    !INI_REAL ini_getf(const TCHAR *Section, const TCHAR *Key, INI_REAL DefValue, const TCHAR *Filename)
    !
      import :: c_char, c_float
      character(kind=c_char, len=1), intent(in) :: Section(*), Key(*), Filename(*)
      real(kind=c_float), intent(in), value :: DefValue
      real(kind=c_float) :: KeyValue
    end function ini_getf
    !
    ! INI_REAL is defined in minIniGlue.h at compilation of minIni C-library (default to float)
    !
    !> ini_getd()
    !! \param Section     the name of the section to search for
    !! \param Key         the name of the entry to find the value of
    !! \param DefValue    the default value in the event of a failed read
    !! \param Filename    the name of the .ini file to read from
    !!
    !! \return            the value located at Key
    function ini_getd(Section, Key, DefValue, Filename) result(KeyValue) bind(C)
    !C-interface:
    !
    !INI_REAL64 ini_getd(const TCHAR *Section, const TCHAR *Key, INI_REAL64 DefValue, const TCHAR *Filename)
    !
      import :: c_char, c_double
      character(kind=c_char, len=1), intent(in) :: Section(*), Key(*), Filename(*)
      real(kind=c_double), intent(in), value :: DefValue
      real(kind=c_double) :: KeyValue
    end function ini_getd
  end interface
end module minIniLib
